﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiniteStateMachine : MonoBehaviour {

    public float lightDuration = 3f;

    Material _trafficLightColor;
    float _maxLightDuration = 3f;

    enum State
    {
        Green,
        Yellow,
        Red
    }

    State _currentState;

    void Awake()
    {
        _trafficLightColor = GetComponent<Renderer>().material;
        StateGreen();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        EnterState();
	}

    void EnterState()
    {
        switch (_currentState)
        {
            case State.Green:
                lightDuration -= Time.deltaTime;
                if (lightDuration <= 0f)
                {
                    StateYellow();
                    lightDuration = _maxLightDuration;
                }

                break;
            case State.Yellow:
                lightDuration -= Time.deltaTime;
                if (lightDuration <= 0f)
                {
                    StateRed();
                    lightDuration = _maxLightDuration;
                }
                break;
            case State.Red:
                lightDuration -= Time.deltaTime;
                if (lightDuration <= 0f)
                {
                    StateGreen();
                    lightDuration = _maxLightDuration;
                }
                break;
            default:
                break;
        }
    }

    void StateGreen()
    {
        _currentState = State.Green;
        Debug.Log("Green");
        _trafficLightColor.color = Color.green;
    }

    void StateYellow()
    {
        _currentState = State.Yellow;
        Debug.Log("Yellow");
        _trafficLightColor.color = Color.yellow;
    }

    void StateRed()
    {
        _currentState = State.Red;
        Debug.Log("Red");
        _trafficLightColor.color = Color.red;
    }


}
